import os, sys

if len(sys.argv) == 1:
	print "Python Normalize_syn_non_syn_sites.py *.normalized"
	sys.exit()

gene_rep_cutoff=10
#print len(sys.argv)
add_dNdS={}
dev_num={}
max={}
min={}
for x in range(1,len(sys.argv)):
	for line in open(sys.argv[x],"rU"):
		if line[:4]=="Gene":continue
		if line.split("\t")[3]=="N/A\n":continue
		if line.split("\t")[3]=="0.0\n":continue
		gene=line.split("\t")[0]
		dNdS=line.split("\t")[3].split("\n")[0]
		try:
			add_dNdS[gene]+=float(dNdS)
			dev_num[gene]+=1
			if float(dNdS) > max[gene]: max[gene]=float(dNdS)
			if float(dNdS) < min[gene]: min[gene]=float(dNdS)
		except:
			add_dNdS[gene]=float(dNdS)
			dev_num[gene]=1
			max[gene]=float(dNdS)
			min[gene]=float(dNdS)
#print averge_dNdS
freq={}
print ("Gene\tAvg_dN/dS")
for g in add_dNdS:
	if dev_num[g] < gene_rep_cutoff: continue
	print(g+"\t"+str(add_dNdS[g]/dev_num[g])+"\t"+str(dev_num[g])+"\t"+str(max[g]-min[g]))
	if dev_num[g] in freq:
		freq[dev_num[g]]+=1
		continue
	freq[dev_num[g]]=1

freq_out=open("freq_"+str(gene_rep_cutoff)+".txt","w")
freq_out.write("Representation\tFrequencey\n")
for nn in freq:
	freq_out.write(str(nn)+"\t"+str(freq[nn])+"\n")
