import os, sys

obs_sites=open(sys.argv[1],"rU")
pos_sites=open(sys.argv[2],"rU")

Syn={}
Non_syn={}
pos_syn={}
pos_non_syn={}
#read in genes from list of possible sites
for line1 in pos_sites:
	if line1[:4] == "Gene": continue
	if line1[:5]=="Total": break
	line=line1.split("_")[0]
	Syn[line]=0
	Non_syn[line]=0
	pos_syn[line]=line1.split("\t")[1]
	pos_non_syn[line]=line1.split("\t")[2].split("\n")[0]
pos_sites.close()

#tally number of unique observed syn and non syn mutations per gene
for lineO in obs_sites:
	#print lineO.split("\t")[5].split("\n")[0].split("Gene_")[1]
	if lineO.split("\t")[5].split("\n")[0].split("Gene_")[1] not in Syn: continue
	#print lineO.split("\t")[5].split("\n")[0]
	if lineO.split("\t")[4]=="SYNONYMOUS_CODING":
		Syn[lineO.split("\t")[5].split("\n")[0].split("Gene_")[1]]+=1
	if lineO.split("\t")[4]=="NON_SYNONYMOUS_CODING":
		Non_syn[lineO.split("\t")[5].split("\n")[0].split("Gene_")[1]]+=1

out_file=open(sys.argv[3],"w")
out_file.write("Gene\tSyn_Obs\tNon_Syn_Obs\tSyn_Pos\tNon_Syn_Pos\n")
for gene in Syn:
	out_file.write(gene+"\t"+str(Syn[gene])+"\t"+str(Non_syn[gene])+"\t"+str(pos_syn[gene])+"\t"+str(pos_non_syn[gene])+"\n")