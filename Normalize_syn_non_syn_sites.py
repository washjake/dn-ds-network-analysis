import os, sys
from decimal import Decimal

if len(sys.argv) != 3:
	print "Python Normalize_syn_non_syn_sites.py <VCF_file> <possible_syn_non_syn_sites.txt>"
	sys.exit()

vcf_file=open(sys.argv[1],"rU")
pos_sites=open(sys.argv[2],"rU")

Syn={}
Non_syn={}

#Populate dictionaries
for line in pos_sites:
	if line[:4] == "Gene": continue
	if line[:5]=="Total": break
	Syn[line.split("\t")[0].split(".")[0]]=0
	Non_syn[line.split("\t")[0].split(".")[0]]=0
pos_sites.close()

#record number of syn and non syn sites from vcf file
for line in vcf_file:
	if line.split("\t")[6][:2]!="AT": continue
	if line.split("\t")[5] == "SYNONYMOUS_CODING":
		Syn[line.split("\t")[6].split("\n")[0]]+=1
	if line.split("\t")[5] == "NON_SYNONYMOUS_CODING":
		Non_syn[line.split("\t")[6].split("\n")[0]]+=1
#print Syn
#print Non_syn

pos_sites=open(sys.argv[2],"rU")
print ("Gene\tNorm_Syn\tNorm_Non_syn\tdN/dS")
for line in pos_sites:
	if line[:4] == "Gene": continue
	if line[:5]=="Total": break
	Gene=line.split("\t")[0].split(".")[0]
	act_syn=Syn[line.split("\t")[0].split(".")[0]]
	act_non_syn=Non_syn[line.split("\t")[0].split(".")[0]]
	pos_syn=float(float(line.split("\t")[1]))
	pos_non_syn=float(float(line.split("\t")[2].split("\n")[0]))
	dN=act_non_syn/pos_non_syn
	dS=act_syn/pos_syn
	#print act_syn/pos_syn
	try: print(Gene+"\t"+str(dS)+"\t"+str(dN)+"\t"+str(dN/dS))
	except: print(Gene+"\t"+str(dS)+"\t"+str(dN)+"\tN/A")
	#Syn[line.split("\t")[0].split(".")[0]]) 