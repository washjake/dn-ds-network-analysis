import os, sys

obs_sites=open(sys.argv[1],"rU")
pos_sites=open(sys.argv[2],"rU")

Syn={}
Non_syn={}
pos_syn={}
pos_non_syn={}
#read in genes from list of possible sites
for line in pos_sites:
	if line[:4] == "Gene": continue
	if line[:5]=="Total": break
	Syn[line.split("\t")[0].split(".")[0]]=0
	Non_syn[line.split("\t")[0].split(".")[0]]=0
	pos_syn[line.split("\t")[0].split(".")[0]]=line.split("\t")[1]
	pos_non_syn[line.split("\t")[0].split(".")[0]]=line.split("\t")[2].split("\n")[0]
pos_sites.close()

#tally number of unique observed syn and non syn mutations per gene
for lineO in obs_sites:
	if lineO.split("\t")[3].split("\n")[0] not in Syn: continue
	#print lineO.split("\t")[5].split("\n")[0]
	if lineO.split("\t")[2]=="SYNONYMOUS_CODING":
		Syn[lineO.split("\t")[3].split("\n")[0]]+=1
	if lineO.split("\t")[2]=="NON_SYNONYMOUS_CODING":
		Non_syn[lineO.split("\t")[3].split("\n")[0]]+=1

out_file=open(sys.argv[3],"w")
out_file.write("Gene\tSyn_Obs\tNon_Syn_Obs\tSyn_Pos\tNon_Syn_Pos\n")
for gene in Syn:
	out_file.write(gene+"\t"+str(Syn[gene])+"\t"+str(Non_syn[gene])+"\t"+str(pos_syn[gene])+"\t"+str(pos_non_syn[gene])+"\n")
