import os, sys

infile=open(sys.argv[1],"rU")
outfile=open(sys.argv[1]+".vcf","w")

outfile.write('##fileformat=VCFv4.0'+'\n'+'##FORMAT=<ID=DP,Number=1,Type=Integer,Description="Read Depth">'+'\n'+'##INFO=<ID=DP,Number=1,Type=Integer,Description="Read Depth">'+'\n')
outfile.write("#CHROM"+"\t"+"POS"+"\t"+"ID"+"\t"+"REF"+"\t"+"ALT"+"\t"+"QUAL"+"\t"+"INFO"+"\t"+"FORMAT"+"\t"+"DP"+"\n")

for line in infile:
	if len(line) < 3: continue
	chr=line.split("\t")[1]
	pos=line.split("\t")[2]
	ref=line.split("\t")[3]
	alt=line.split("\t")[4]
	qual=line.split("\t")[5]
	DP=line.split("\t")[6]
	#print (chr+" "+pos+" "+ref+" "+alt+" "+qual+" "+DP)
	outfile.write(chr+"\t"+pos+"\t"+"."+"\t"+ref+"\t"+alt+"\t"+qual+"\t"+"DP="+DP+"\t"+"DP"+"\t"+DP+"\n")