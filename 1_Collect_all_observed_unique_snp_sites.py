import os, sys
import datetime
#print len(sys.argv)
unique_snps=set()
count=0
time_now =datetime.datetime.now()
print str(count)+"/"+str(len(sys.argv)-1)+" files completed. Elapsed time: "+str(datetime.datetime.now() - time_now)
for x in range(1,len(sys.argv)):
	for line in open(sys.argv[x],"rU"):
		if line[:6]=="#CHROM": continue#print line.split("\n")[0]
		snp_details=line.split("\t")[0]+"\t"+line.split("\t")[1]+"\t"+line.split("\t")[5]+"\t"+line.split("\t")[6].split("\n")[0]
		#print snp_details
		unique_snps.add(snp_details)
	count+=1
	print str(count)+"/"+str(len(sys.argv)-1)+" files completed.  Total unique SNPs: "+str(len(unique_snps))+" Elapsed time: "+str(datetime.datetime.now() - time_now)
out_file=open("Collected_unique_snp_sites.txt","w")
for snp in unique_snps:
	out_file.write(snp+"\n")
out_file.close()