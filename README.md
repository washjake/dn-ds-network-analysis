# README #

Note: pipeline is not fully tested, modifications will be needed.

### Running the pipeline ###

##1. Generate list of possible synonymous and non-synonymous mutations. To do this you need the coding/transcript sequences from a reference genome. The example command below uses Arabidopsis thaliana. You will need to log on to Gavin's server to run his script for doing this. Example command:

	./find_syn_nonsyn_sites Athaliana_167_TAIR10.cds_primaryTranscriptOnly.fa

##2. Obtain or create vcf files based on variation from reference genome. In the case of Arabidopsis http://1001genomes.org/data/GMI-MPI/releases/v3.1/1001genomes_snp-short-indel_only_ACGTN.vcf.gz was downloaded from the Arabidopsis 1001.

##3. Use SnpEff (http://snpeff.sourceforge.net/index.html) or other variant calling and extraction software to filter vcf to synonymous and non-synonymous mutations.  

	java -Xmx4g -jar ../snpEff.jar -classic -formatEff athalianaTair10 quality_variant_vcf_108_TAIR10.vcf > classic.108.vcf
	java -jar ../SnpSift.jar filter "( EFF[*].EFFECT = 'NON_SYNONYMOUS_CODING' ) | ( EFF[*].EFFECT = 'SYNONYMOUS_CODING' )" classic.108.vcf > classic.108.filtered
	java -jar ../SnpSift.jar extractFields filtered_classic.108.vcf CHROM POS REF ALT QUAL "EFF[0].EFFECT" "EFF[0].GENE" > classic.108.filtered.extracted

##4. python 1_Collect_all_observed_unique_snp_sites.py 1001genomes_snp-short-indel_only_ACGTN.vcf.gz.annotated.filtered.extracted

##5 python 2b_Tally_observed_syn_non_syn_sites_per_gene.py Collected_unique_snp_sites.txt Athal_167_TAIR10.cds_primary_syn_nonsyn_gavin_script.txt

?????
##4. Normalize synonymous and non-synonymous sites based on possible mutations determined in step 1

	python Normalize_syn_non_syn_sites.py classic.108.filtered.extracted Athal_167_TAIR10.cds_primary_syn_nonsyn_gavin_script.txt > classic.108.filtered.extracted.normalized