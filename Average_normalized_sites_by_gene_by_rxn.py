#This script takes as input the output of Average_normalized_sites_by_gene_across_species.py along with a file mapping genes to reactions(enzymes).

import os, sys

if len(sys.argv) != 3:
	print "Python Normalize_syn_non_syn_sites.py <out_put from Ave...across_species.py> <genes_mapped_to_rxns.txt>"
	sys.exit()

#read in output from previous script
avg_by_gene={}
for line in open(sys.argv[1],"rU"):
	if line[:4]=="Gene":continue
	gene=line.split("\t")[0]
	dNdS=line.split("\t")[1].split("\n")[0]
	avg_by_gene[gene]=dNdS

#print avg_by_gene
add_dNdS={}
dev_num={}
max={}
min={}
for line1 in open(sys.argv[2],"rU"):
	rxn=line1.split("\t")[0]
	gene=line1.split("\t")[1].split("\n")[0]
	try:
		add_dNdS[rxn]+=float(avg_by_gene[gene])
		dev_num[rxn]+=1
		if float(avg_by_gene[gene]) > max[rxn]: max[rxn]=float(avg_by_gene[gene])
		if float(avg_by_gene[gene]) < min[rxn]: min[rxn]=float(avg_by_gene[gene])
	except:
		try: 
			add_dNdS[rxn]=float(avg_by_gene[gene])
			dev_num[rxn]=1
			max[rxn]=float(avg_by_gene[gene])
			min[rxn]=float(avg_by_gene[gene])
		except KeyError: continue
	#if rxn=="R06259_c":
	#	print (rxn+"\t"+gene+"\t"+avg_by_gene[gene])

print ("Rxn\tAvg_dN/dS\tnum_genes_in_Rxn\tMax-min")
for R in add_dNdS:
	print(R+"\t"+str(add_dNdS[R]/dev_num[R])+"\t"+str(dev_num[R])+"\t"+str(max[R]-min[R])[:6])