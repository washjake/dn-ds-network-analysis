import os, sys

infile= sys.argv[1]

cmd1="java -Xmx4g -jar /data/jwashbur/snpEff/snpEff.jar -classic -formatEff athalianaTair10 "+infile+" > "+infile+".vcf"
print (cmd1)
os.system(cmd1)

cmd2="""java -jar /data/jwashbur/snpEff/SnpSift.jar filter "( EFF[*].EFFECT = 'NON_SYNONYMOUS_CODING' ) | ( EFF[*].EFFECT = 'SYNONYMOUS_CODING' )" """+infile+'.vcf > '+infile+'.vcf.filtered'
print (cmd2)
os.system(cmd2)

cmd5='java -jar /data/jwashbur/snpEff/SnpSift.jar extractFields '+infile+'.vcf.filtered'+' CHROM POS REF ALT QUAL "EFF[0].EFFECT" "EFF[0].GENE" > Syn_non_syn_called/'+infile+'.vcf.filtered.extracted'
print (cmd5)
os.system(cmd5)